///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file animal.hpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Jonah Bobilin <jbobilin@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   4 March 2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#pragma once

using namespace std;

namespace animalfarm {

//enums
enum Gender { MALE, FEMALE, UNKNOWN };

enum Color { BLACK, WHITE, RED, BLUE, GREEN, PINK, SILVER, YELLOW, BROWN};

//animal class
class Animal {
public:
   Animal();
   ~Animal();

public:
	enum Gender gender;
	string      species;

public:
	virtual const string speak() = 0;
	
	void printInfo();
	
	string colorName  (enum Color color);
	string genderName (enum Gender gender);

   //random functions
   static const Gender  getRandomGender();
   static const Color   getRandomColor();
   static const bool    getRandomBool();
   static const float   getRandomWeight(const float from, const float to);
   static const string  getRandomName();
};


} // namespace animalfarm
