///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file animalfactory.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Jonah Bobilin <jbobilin@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   6 March 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include "animalfactory.hpp"

using namespace std;

namespace animalfarm {

//create animal factory class
Animal* AnimalFactory::getRandomAnimal(){
   Animal* newAnimal = NULL;
 
   //generate random animal
   int i = int(rand()%6);

   switch(i) {
      case 0:
         newAnimal = new Cat(Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender());
         break;
      case 1:
         newAnimal = new Dog(Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender());
         break;
      case 2:
         newAnimal = new Nunu(Animal::getRandomBool(), RED, Animal::getRandomGender());
         break;
      case 3:
         newAnimal = new Aku(Animal::getRandomWeight(14,76), SILVER, Animal::getRandomGender());
         break;
      case 4:
         newAnimal = new Palila(Animal::getRandomName(), YELLOW, Animal::getRandomGender());
         break;
      case 5:
         newAnimal = new Nene(Animal::getRandomName(), BROWN, Animal::getRandomGender());
         break;
   }
    
   return newAnimal;
}
	
} // namespace animalfarm
