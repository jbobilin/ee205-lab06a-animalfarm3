///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Jonah Bobilin <jbobilin@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   6 March 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <array>
#include <list>
#include "time.h"
#include "animal.hpp"
#include "animalfactory.hpp"
#include "fish.hpp"
#include "bird.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "aku.hpp"
#include "nunu.hpp"
#include "palila.hpp"
#include "nene.hpp"

//namespaces
using namespace std;

using namespace animalfarm;

int main() {
   
   //seed rand
   srand(time(NULL));

   //print title
	cout << "Welcome to Animal Farm 3" << endl;
   
   ///////////
   // ARRAY //
   ///////////
   //run loop 25 times to populate array
   int i;
   array<Animal*, 30>animalArray;
   animalArray.fill(NULL);
   for (i=0;i<25;i++){
      animalArray[i] = AnimalFactory::getRandomAnimal();
   }
   
   //print array info
   cout << endl << "Array of Animals:" << endl;
   cout << "  Is it empty: " << std::boolalpha << animalArray.empty() << endl;
   cout << "  Number of elements: " << animalArray.size() << endl;
   cout << "  Max size: " << animalArray.max_size() << endl;

   //print speaks
   for(Animal* animal : animalArray){
      if (animal != NULL)
         cout << animal->speak() << endl;
   }

   //delete array
   int foo=animalArray.size();
   for(i=0;i<foo;i++){
      delete animalArray[i];
   }


   //////////
   // LIST //
   //////////
   //run loop 25 times to populate list
   list<Animal* > animalList;
   for (i=0;i<25;i++){
      animalList.push_front(AnimalFactory::getRandomAnimal());
   }
   
   //print list info
   cout << endl << "List of Animals:" << endl;
   cout << "  Is it empty: " << std::boolalpha << animalList.empty() << endl;
   cout << "  Number of elements: " << animalList.size() << endl;
   cout << "  Max size: " << animalList.max_size() << endl;

   //print speaks
   for(Animal* animal : animalList){
      cout << animal->speak() << endl;
   }
   
   //delete list
   while (!animalList.empty()){
      delete animalList.front();
      animalList.pop_front();
   }
  
   //add newline
   cout << endl;
   return 0;
}
