///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file nunu.hpp
/// @version 1.0
///
/// Exports data about all nunu
///
/// @author Jonah Bobilin <jbobilin@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   6 March 2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

#include "fish.hpp"

using namespace std;


namespace animalfarm {

class Nunu : public Fish {
public:
	bool isNative;

	Nunu(bool newIsNative, enum Color newColor, enum Gender newGender );

	void printInfo();
};

} // namespace animalfarm

