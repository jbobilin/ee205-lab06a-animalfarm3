///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file nene.cpp
/// @version 1.0
///
/// Exports data about all nene birds
///
/// @author Jonah Bobilin <jbobilin@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   6 March 2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "nene.hpp"

using namespace std;

namespace animalfarm {
	
Nene::Nene( string newTagID, enum Color newColor, enum Gender newGender ) {
	gender = newGender;        
	species = "Branta sandvicensis";
	featherColor = newColor;       
	tagID = newTagID; 
   isMigratory = true;
}

const string Nene::speak(){
   return string("Nay, nay");
}

// Print our name first... then print other information
void Nene::printInfo() {
	cout  << "Nene" << endl;
   cout << "   Tag ID = [" << tagID << "]" << endl;
   Bird::printInfo();
}

} // namespace animalfarm
