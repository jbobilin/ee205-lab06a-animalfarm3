///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Jonah Bobilin <jbobilin@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   6 March 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include <time.h>
#include "animal.hpp"

using namespace std;

namespace animalfarm {

//constructor
Animal::Animal(){
   cout << ".";
}

//destructor
Animal::~Animal(){
   cout << "x";
}

//printinfo for animals
void Animal::printInfo() {
	cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}

//gender enum
string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE:    return string("Male"); break;
      case FEMALE:  return string("Female"); break;
      case UNKNOWN: return string("Unknown"); break;
   }

   return string("Really, really Unknown");
};

//function to generate random gender
const Gender Animal::getRandomGender(){
   if (rand()%2)
      return MALE;
   else
      return FEMALE;
}

//color enum
string Animal::colorName (enum Color color) {
	// swtich case implemented to print colors
   switch (color) {
   case BLACK:
      return string("Black"); break;
   case WHITE:
      return string("White"); break;
   case RED:
      return string("Red"); break;
   case BLUE:
      return string("Blue"); break;
   case GREEN:
      return string("Green"); break;
   case PINK:
      return string("Pink"); break;
   case SILVER:
      return string("Silver"); break;
   case YELLOW:
      return string("Yellow"); break;
   case BROWN:
      return string("Brown"); break;
   default:
      return string("not found"); break;
   }

   return string("ERROR: very not found!");
};


//function to generate random color
const Color Animal::getRandomColor(){
   switch(rand()%9){
   case 0:
      return BLACK;
   case 1:
      return WHITE;
   case 2:
      return RED;
   case 3:
      return BLUE;
   case 4:
      return GREEN;
   case 5:
      return PINK;
   case 6:
      return SILVER;
   case 7:
      return YELLOW;
   case 8:
      return BROWN;
   default:
      return BLACK;
   }
}


//function to generate random bool
const bool Animal::getRandomBool(){
   if (rand()%2)
      return true;
   else
      return false;
}


//function to generate random weight between two floats
const float Animal::getRandomWeight(const float from, const float to){
   return ((to - from) * ((float)rand() / (float)RAND_MAX) + from);

}


//function to generate random name
const string Animal::getRandomName(){
   //init variables
   int length;
   int i;
   int r;
  
   //set length of names between 4 and 8
   length = rand()%5 + 4;
   char randomName[length];

   //uppercase
   randomName[0] ='A'+rand()%26;

   // lowercase loop
   for (i=1;i<length;i++) {
      r = rand()%26;
      randomName[i] ='a' + r;
   }
   randomName[length]='\0';// terminate char array
   
   return randomName;
}


} // namespace animalfarm
